# Changelog

<!-- ## Unreleased -->

## 0.1.0 - Development
### Added
- Addon options accessible with `\ + d`
- Basic damage output panel

# MeowMeter

DPS meter addon for Isleward.

## Usage

Userscripts are in `dist/`.
Install with ViolentMonkey or AddonManager.

Press `\ + d` to open the options panel.

## Inspiration
<!-- Other references
https://flamesofthemist.com/arcdps-guide Some documentation of ArcDPS
-->

* ArcDPS for GW2
<!-- * Looking at pictures of Details! / Recount / etc lol -->

## Development

* `yarn && yarn dev` builds to `build/` and hosts at `localhost:8080`
* `yarn build-ci` builds to `dist/`
* `yarn version` for releases

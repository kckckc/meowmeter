import store, { TransactFn } from 'store2';

const defaults: Record<string, any> = {};

const PREFIX = 'meowmeter.';

const observers: Record<string, ((value: any) => void)[]> = {};
const onKeyChanged = (key: string, cb: (value: any) => void) => {
	if (!observers[key]) {
		observers[key] = [];
	}

	observers[key].push(cb);

	cb(getValue(key));
};
const offKeyChanged = (key: string, cb: (value: any) => void) => {
	if (!observers[key]) return;

	observers[key] = observers[key].filter((o) => o !== cb);
};

const getValue = (key: string) => {
	const persistedValue = store.get(PREFIX + key);
	return persistedValue ?? getDefaultValue(key);
};
const setValue = (key: string, value: any) => {
	store.set(PREFIX + key, value);

	if (observers[key]) {
		observers[key].forEach((o) => o(value));
	}
};
const transactValue = (key: string, fn: TransactFn) => {
	store.transact(PREFIX + key, fn, getDefaultValue(key));

	if (observers[key]) {
		observers[key].forEach((o) => o(getValue(key)));
	}
};

const getDefaultValue = (key: string) => {
	return defaults[key] ?? undefined;
};

export default {
	onKeyChanged,
	offKeyChanged,
	getValue,
	setValue,
	transactValue,
	defaults,
};

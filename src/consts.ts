export const elements = ['arcane', 'fire', 'frost', 'holy', 'poison'];
export const CHANGELOG_LINK =
	'https://gitlab.com/kckckc/meowmeter/-/blob/main/CHANGELOG.md';

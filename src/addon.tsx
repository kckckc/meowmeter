import React from 'react';
import { controls } from './controls';
import { DamagePanel } from './panels/DamagePanel';
import { renderer } from './renderer/renderer';
import settings from './settings';
import tracker from './tracker';
import { checkVersion } from './util/checkVersion';
import { hello } from './util/hello';

const addon = {
	id: typeof ADDON_ID === 'undefined' ? 'meowmeter' : ADDON_ID,
	name: 'MeowMeter',

	init: function () {
		controls.init();
		renderer.init();

		tracker.init();

		// TEMP: For faster testing
		// toggleOptionsPanel();
		// makeDemoPanel();
		// makeDamagePanel();

		// Restore panels
		const numPanels = settings.getValue('numPanels');
		for (let i = 0; i < numPanels; i++) {
			renderer.addElement(
				`damage.${i}`,
				<DamagePanel id={i.toString()} />
			);
		}

		hello();
		checkVersion();
	},

	cleanup: function () {
		tracker.cleanup();

		controls.cleanup();
		renderer.cleanup();
	},
};

export { addon };

// Entry/loader file
// Should load in AddonManager, userscript managers, and desktop
// @ts-nocheck

import { addon } from './addon';

// prettier-ignore
(a=>{let p=_=>{let m=(typeof unsafeWindow==='undefined'?globalThis:unsafeWindow)[typeof ADDON_MANAGER=='undefined'?'addons':ADDON_MANAGER];m?m.register(a):setTimeout(p,200)};p()})(addon);

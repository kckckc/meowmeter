import React, { ReactElement } from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import { PanelOrderProvider } from '../components/PanelOrderProvider';
import styles from './renderer.css';

let container: HTMLDivElement;

type PanelEntry = { el: ReactElement; key: string };
let panels: PanelEntry[] = [];

const init = () => {
	const iwdContainer = document.querySelector('.ui-container');
	container = document.createElement('div');
	container.className = `dps-container ${styles.container}`;
	iwdContainer?.appendChild(container);
	doRender();
};

const addElement = (key: string, el: ReactElement) => {
	panels.push({
		el,
		key,
	});
	doRender();
};

const removeElement = (key: string) => {
	panels = panels.filter((p) => p.key !== key);
	doRender();
};

const hasElement = (key: string) => {
	return panels.some((p) => p.key === key);
};

const doRender = () => {
	render(
		<PanelOrderProvider>
			{panels.map((p) => React.cloneElement(p.el, { key: p.key }))}
		</PanelOrderProvider>,
		container
	);
};

const cleanup = () => {
	unmountComponentAtNode(container);
	container.parentElement?.removeChild(container);
};

const renderer = {
	init,
	addElement,
	hasElement,
	removeElement,
	cleanup,
};

export { renderer };

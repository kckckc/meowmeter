import React from 'react';
import { DemoPanel } from '../panels/DemoPanel';
import { OptionsPanel } from '../panels/OptionsPanel';
import { renderer } from './renderer';

export const toggleOptionsPanel = () => {
	if (!renderer.hasElement('options')) {
		renderer.addElement('options', <OptionsPanel />);
	} else {
		renderer.removeElement('options');
	}
};

const getNextId = (name: string, id = 1): string => {
	return `${name}${getNextIdRaw(name, id)}`;
};
const getNextIdRaw = (name: string, id = 1): number => {
	if (renderer.hasElement(`${name}${id}`)) {
		return getNextIdRaw(name, id + 1);
	}
	return id;
};

export const makeDemoPanel = () => {
	const id = getNextId('demo');
	renderer.addElement(id, <DemoPanel name={id} />);
};

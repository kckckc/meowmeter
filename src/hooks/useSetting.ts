import { useCallback, useEffect, useState } from 'react';
import settings from '../settings';

export const useSetting = <T>(
	key: string
): [T, (arg: T | ((prev: T) => T)) => void] => {
	// Keep an internal value for the hook
	const [value, setInternal] = useState<T>(() => {
		return settings.getValue(key);
	});

	// Listen for changes to the setting, and update internal
	useEffect(() => {
		const cb = (newVal: T) => {
			setInternal(newVal);
		};

		settings.onKeyChanged(key, cb);

		return () => {
			settings.offKeyChanged(key, cb);
		};
	}, [key]);

	// Custom setter for the store
	// Triggers the listener which updates internal
	const setterFn = useCallback(
		(update) => {
			if (typeof update === 'function') {
				settings.transactValue(key, update);
				return;
			}

			settings.setValue(key, update);
		},
		[key]
	);

	return [value, setterFn];
};

import { useEffect, useState } from 'react';
import { requireAsync } from '../util/requireAsync';

export const useRequire = <T>(args: string): T | null => {
	const [data, setData] = useState<T | null>(null);

	useEffect(() => {
		const doRequire = async () => {
			const result = await requireAsync(args);
			setData(result);
		};

		doRequire();
	}, [args]);

	return data;
};

import { useEffect, useState } from 'react';
import tracker from '../tracker';

export const useTracker = () => {
	const [, rerender] = useState({});

	useEffect(() => {
		const subscriber = () => {
			rerender({});
		};

		tracker.subscribe(subscriber);

		return () => {
			tracker.unsubscribe(subscriber);
		};
	}, []);

	return tracker;
};

import { useState } from 'react';
import { useRequire } from './useRequire';

interface ObjectsModule {
	objects: {
		id: number | string;
		name: string;
	}[];
}

const buildName = (n: string, i: number) => `${n} (${i})`;

export const useGetName = (): ((id: string) => string) => {
	const objects = useRequire<ObjectsModule>('js/objects/objects');
	const [nameCache, setNameCache] = useState<Record<string, string>>({});
	const [nameCacheTaken, setNameCacheTaken] = useState<
		Record<string, boolean>
	>({});

	return (id: string) => {
		if (nameCache[id]) return nameCache[id];

		if (!objects) return id;

		const obj = objects?.objects.find((o) => o.id?.toString() === id);
		if (!obj) return id;

		// First of type
		let name = obj.name;
		if (!nameCacheTaken[name]) {
			setNameCache((prev) => ({ ...prev, [id]: name }));
			setNameCacheTaken((prev) => ({ ...prev, [name]: true }));
			return obj.name;
		}

		let count = 2;
		name = buildName(obj.name, count);
		while (nameCacheTaken[name]) {
			count += 1;
			name = buildName(obj.name, count);
		}
		setNameCache((prev) => ({ ...prev, [id]: name }));
		setNameCacheTaken((prev) => ({ ...prev, [name]: true }));
		return name;
	};
};

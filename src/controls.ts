import { toggleOptionsPanel } from './renderer/makePanel';

let timeout: ReturnType<typeof setTimeout>;

const getWindow = () =>
	typeof unsafeWindow === 'undefined' ? globalThis : unsafeWindow;

const keys = new Set();
const onKeyDown = (e: KeyboardEvent) => {
	keys.add(e.key);

	if (e.key === '\\' || e.key === 'd') {
		if (keys.has('\\') && keys.has('d')) {
			toggleOptionsPanel();
			e.stopPropagation();
		}
	}
};
const onKeyUp = (e: KeyboardEvent) => {
	keys.delete(e.key);
};

const init = () => {
	// Waddon slash action
	timeout = setTimeout(() => {
		(getWindow() as Window)?.Waddon?.registerAction({
			id: 'meowmeter:open',
			name: 'MeowMeter: Open Options',
			callback: () => {
				toggleOptionsPanel();
			},
		});
	}, 5000);

	document.addEventListener('keydown', onKeyDown);
	document.addEventListener('keyup', onKeyUp);
};

const cleanup = () => {
	// In case it hasn't fired already
	clearTimeout(timeout);

	document.removeEventListener('keydown', onKeyDown);
	document.removeEventListener('keyup', onKeyUp);
};

const controls = {
	init,
	cleanup,
};

export { controls };

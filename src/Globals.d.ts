// CSS module imports
declare module '*.css';

// Addon Manager addon
declare const ADDON_ID: string;

// Webpack DefinePlugin
declare const __ADDONVERSION__: string;

// IWD events
type OnGetDamageMessageOld = {
	id: number;
	source: number;
	masterId?: number;
	masterSource?: number;
	amount: number;
	element: string;
	crit?: boolean;
	heal?: boolean;
	event?: boolean;
};

type OnGetDamageMessage = {
	target: number;
	source: number;
	targetMaster?: number;
	sourceMaster?: number;
	amount: number;
	element: string;
	crit?: boolean;
	heal?: boolean;
};

declare interface IwdEvents {
	on(
		event: 'onGetDamage',
		listener: (msg: OnGetDamageMessageOld) => void
	): void;
	on(event: string, listener: (...args: any[]) => void): void;
	off(event: string, listener: (...args: any[]) => void): void;
}

declare interface IwdObject {
	id: number;
}
declare interface IwdPlayer extends IwdObject {}

// Waddon
interface GlobalWaddon {
	registerAction(action: RegisterActionProps): void;
}
interface RegisterActionProps {
	id: string;
	name: string;
	callback: () => void;
}

// Window
declare interface Window {
	addons: {
		events: IwdEvents;
	};
	player: IwdPlayer;
	Waddon?: GlobalWaddon;
	require: (args: string[], cb: (...args: any[]) => void) => void;
}

// unsafeWindow in userscript loaders like ViolentMonkey
declare const unsafeWindow: Window | undefined;

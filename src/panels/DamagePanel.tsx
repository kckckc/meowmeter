import { WhereOptions } from '@kckckc/amalgam';
import React from 'react';
import { Panel } from '../components/Panel';
import { PanelRow } from '../components/PanelRow';
import { useGetName } from '../hooks/useGetName';
import { useSetting } from '../hooks/useSetting';
import { useTracker } from '../hooks/useTracker';
import { renderer } from '../renderer/renderer';
import { DamageTypeDetails } from '../tracker';
import { capitalize } from '../util/capitalize';
import { getWindow } from '../util/getWindow';
import { abbreviateNumber, percentString } from '../util/numbers';
import { summarizeDamage } from '../util/summarizeDamage';
import styles from './DamagePanel.css';
import { DamagePanelContext } from './DamagePanelContext';

const GROUP_SEP = ',';

export interface DamagePanelProps {
	id: string;
}

export interface PanelOptions {
	hidden?: boolean;
	showDps?: boolean;
	showIn?: boolean;
	showHealing?: boolean;
	groupCrits?: boolean;
	groupElements?: boolean;
}

const parseOptions = (s: string) => {
	let v: PanelOptions;
	try {
		v = JSON.parse(s);
	} catch (e) {
		v = {};
	}
	return v;
};

const rightInfo = (details: DamageTypeDetails, options: PanelOptions) => {
	const { showDps } = options;

	let percentPart = '';
	if (typeof details.percent !== 'undefined') {
		percentPart = `, ${percentString(details.percent)}`;
	}

	return `${abbreviateNumber(details.amount)} (${abbreviateNumber(
		showDps ? details.dps : details.dpt
	)}/${showDps ? 's' : 't'}${percentPart})`;
};

export const DamagePanel: React.FC<DamagePanelProps> = ({ id }) => {
	const tracker = useTracker();
	const getName = useGetName();

	const [rawOptions, setRawOptions] = useSetting<string>('panel.' + id);
	const options = parseOptions(rawOptions);
	const setOptions = (o: PanelOptions) => {
		setRawOptions(JSON.stringify(o));
	};

	const { hidden, showHealing, showIn, groupCrits, groupElements } = options;

	const panelName =
		(showHealing ? 'Healing' : 'Damage') + ' ' + (showIn ? 'In' : 'Out');

	// Configure filter
	const filter: WhereOptions = {
		type: { $eq: 'damage' },
	};
	if (showIn) {
		filter['data.target'] = { $eq: getWindow()?.player?.id };
	} else {
		filter['data.source'] = { $eq: getWindow()?.player?.id };
	}
	if (showHealing) {
		filter['data.heal'] = { $eq: true };
	} else {
		filter['data.heal'] = { $neq: true };
	}

	const found = tracker.stack.findGrouped({
		whereAll: filter,
		group: (i) => {
			const dmg = i?.data?.data;
			// let element = dmg?.element ?? 'physical';
			// return `${element}`;

			const id = showIn ? dmg.source : dmg.target;

			const element = groupElements
				? dmg?.element ?? 'physical'
				: 'ungrouped';
			const crit = groupCrits ? (dmg?.crit ? 'y' : 'n') : 'ungrouped';

			return [id, element, crit].join(GROUP_SEP);
		},
	});
	const overall = summarizeDamage(Object.values(found).flat());

	let data: Record<string, DamageTypeDetails> = {};
	if (overall) {
		data = Object.fromEntries(
			Object.entries(found)
				.map(([group, events]) => {
					const summary = summarizeDamage(events, overall.amount);
					return [group, summary];
				})
				// Remove null summaries (no events)
				.filter((x) => !!x[1])
		);
		console.log(data);
	}

	if (hidden) {
		return null;
	}

	return (
		<Panel
			style={{ minWidth: '400px' }}
			saveAs={'meowmeter.damage.' + id}
			contextMenu={
				<DamagePanelContext
					id={id}
					options={options}
					setOptions={setOptions}
				/>
			}
			header={panelName}
			onClose={() => {
				setOptions({ ...options, hidden: !options.hidden });
			}}
		>
			{!overall && <PanelRow>waiting...</PanelRow>}
			{overall &&
				Object.entries(data)
					.sort(([, a], [, b]) => (b.percent ?? 0) - (a.percent ?? 0))
					.map(([group, details]) => {
						const [stringId, element, stringCrit] =
							group.split(GROUP_SEP);

						let label = capitalize(getName(stringId));

						if (element !== 'ungrouped') {
							label += ' [' + capitalize(element) + ']';
						}
						if (stringCrit === 'y') {
							label += ' [Crit]';
						}

						return (
							<PanelRow key={group} percent={details.percent}>
								<div className={styles.damageRow}>
									<div>{label}</div>
									<div className={styles.right}>
										{rightInfo(details, options)}
									</div>
								</div>
							</PanelRow>
						);
					})}
		</Panel>
	);
};

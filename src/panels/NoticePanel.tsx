import React, { ReactNode } from 'react';
import { Panel } from '../components/Panel';
import { renderer } from '../renderer/renderer';

interface NoticePanelProps {
	name: string;
	header?: string;
	children?: ReactNode;
}

export const NoticePanel: React.FC<NoticePanelProps> = ({
	name,
	header = 'Notice',
	children,
}) => {
	return (
		<Panel
			openCentered
			header={header}
			onClose={() => renderer.removeElement(name)}
		>
			{children}
		</Panel>
	);
};

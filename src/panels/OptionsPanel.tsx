import { CM, ContextMenu } from '@kckckc/isleward-util';
import React, { useState } from 'react';
import { Panel } from '../components/Panel';
import { CHANGELOG_LINK } from '../consts';
import { useSetting } from '../hooks/useSetting';
import { makeDemoPanel } from '../renderer/makePanel';
import { renderer } from '../renderer/renderer';
import settings from '../settings';
import { getKaomoji } from '../util/hello';
import { DamagePanel, PanelOptions } from './DamagePanel';
import styles from './OptionsPanel.css';

interface OptionsPanelProps {}

settings.defaults.numPanels = 0;

// Helpers
const getOptions = (panel: number) => {
	const s = settings.getValue('panel.' + panel);
	let v: PanelOptions;
	try {
		v = JSON.parse(s);
	} catch (e) {
		v = {};
	}
	return v;
};
const setOptions = (panel: number, options: PanelOptions) => {
	settings.setValue('panel.' + panel, JSON.stringify(options));
};
const mergeOptions = (panel: number, options: PanelOptions) => {
	const prev = getOptions(panel);
	setOptions(panel, { ...prev, ...options });
};

export const OptionsPanel: React.FC<OptionsPanelProps> = () => {
	const [, rerender] = useState({});

	const [cat] = useState(() => {
		return getKaomoji();
	});

	const [numPanels, setNumPanels] = useSetting<number>('numPanels');

	// Actions
	const setAllShowDps = (v: boolean) => {
		for (let i = 0; i < numPanels; i++) {
			mergeOptions(i, { showDps: v });
		}
	};
	const getHidden = (panel: number) => {
		const o = getOptions(panel);
		return !!o.hidden;
	};
	const toggleHidden = (panel: number) => {
		mergeOptions(panel, { hidden: !getHidden(panel) });
		rerender({});
	};

	return (
		<Panel
			saveAs="meowmeter.options"
			header={
				<>
					MeowMeter v{__ADDONVERSION__}
					<span className={styles.plain}>{cat}</span>
				</>
			}
			onClose={() => renderer.removeElement('options')}
		>
			<ContextMenu embedded>
				<CM>
					<CM.Button
						onClick={() => {
							renderer.addElement(
								`damage.${numPanels}`,
								<DamagePanel id={numPanels.toString()} />
							);
							setNumPanels(numPanels + 1);
						}}
					>
						+ New Panel
					</CM.Button>

					<CM.Expand label={'Toggle panels'}>
						{new Array(numPanels).fill(0).map((_, i) => (
							<CM.Checkbox
								key={i}
								checked={!getHidden(i)}
								onChange={() => {
									toggleHidden(i);
								}}
							>
								Panel {i + 1}
							</CM.Checkbox>
						))}
					</CM.Expand>

					<CM.Expand label="Change all panels">
						<CM.Button onClick={() => setAllShowDps(false)}>
							Show DPT
						</CM.Button>
						<CM.Button onClick={() => setAllShowDps(true)}>
							Show DPS
						</CM.Button>
					</CM.Expand>

					<CM.Button
						onClick={() => {
							for (let i = 0; i < numPanels; i++) {
								setOptions(i, {});
								renderer.removeElement('damage.' + i);
							}
							setNumPanels(0);
						}}
					>
						Reset all panels
					</CM.Button>

					<CM.Button onClick={() => makeDemoPanel()}>
						+ Test Panel
					</CM.Button>

					<CM.Button
						onClick={() => {
							window.open(CHANGELOG_LINK, '_black');
						}}
					>
						[Changelog]
					</CM.Button>
				</CM>
			</ContextMenu>
		</Panel>
	);
};

import { CM } from '@kckckc/isleward-util';
import React from 'react';
import { PanelOptions } from './DamagePanel';

interface DamagePanelContextProps {
	id: string;
	options: PanelOptions;
	setOptions: (p: PanelOptions) => void;
}

const PRESETS = [
	{
		name: 'Damage Out',
		options: {
			showIn: false,
			showHealing: false,
		},
	},
	{
		name: 'Damage In',
		options: {
			showIn: true,
			showHealing: false,
		},
	},
	{
		name: 'Healing Out',
		options: {
			showIn: false,
			showHealing: true,
		},
	},
	{
		name: 'Healing In',
		options: {
			showIn: true,
			showHealing: true,
		},
	},
];

export const DamagePanelContext: React.FC<DamagePanelContextProps> = ({
	id,
	options,
	setOptions,
}) => {
	return (
		<CM>
			<CM.Label>
				<strong style={{ width: '100%', textAlign: 'center' }}>
					Panel {parseInt(id) + 1} Options
				</strong>
			</CM.Label>
			<CM.Expand label="Presets">
				{PRESETS.map((p) => {
					return (
						<CM.Button
							key={p.name}
							onClick={() => setOptions(p.options)}
						>
							{p.name}
						</CM.Button>
					);
				})}
			</CM.Expand>
			<CM.Checkbox
				checked={!!options.showDps}
				onChange={() =>
					setOptions({ ...options, showDps: !options.showDps })
				}
			>
				Show DPS
			</CM.Checkbox>
			<CM.Checkbox
				checked={!!options.showIn}
				onChange={() =>
					setOptions({ ...options, showIn: !options.showIn })
				}
			>
				Show incoming
			</CM.Checkbox>
			<CM.Checkbox
				checked={!!options.showHealing}
				onChange={() =>
					setOptions({
						...options,
						showHealing: !options.showHealing,
					})
				}
			>
				Show healing
			</CM.Checkbox>
			<CM.Checkbox
				checked={!!options.groupCrits}
				onChange={() =>
					setOptions({ ...options, groupCrits: !options.groupCrits })
				}
			>
				Group by crits
			</CM.Checkbox>
			<CM.Checkbox
				checked={!!options.groupElements}
				onChange={() =>
					setOptions({
						...options,
						groupElements: !options.groupElements,
					})
				}
			>
				Group by elements
			</CM.Checkbox>
		</CM>
	);
};

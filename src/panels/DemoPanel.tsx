import { CM } from '@kckckc/isleward-util';
import React, { useEffect, useState } from 'react';
import { Panel } from '../components/Panel';
import { PanelRow } from '../components/PanelRow';
import { renderer } from '../renderer/renderer';

interface DemoPanelProps {
	name: string;
}

const calc = (x: number) => {
	return (Math.sin(0 + x) / 2 + 0.5) * 0.9 + 0.05;
};

export const DemoPanel: React.FC<DemoPanelProps> = ({ name }) => {
	const [offset, setOffset] = useState(0);

	useEffect(() => {
		const interval = setInterval(() => {
			setOffset((o) => {
				let o2 = o - 0.075;
				if (o2 > Math.PI * 2) {
					o2 -= Math.PI * 2;
				}
				return o2;
			});
		}, 50);

		return () => {
			clearInterval(interval);
		};
	});

	return (
		<Panel
			header={'Demo'}
			contextMenu={
				<CM>
					<CM.Label>:)</CM.Label>
				</CM>
			}
			onClose={() => renderer.removeElement(name)}
		>
			{/* <PanelRow percent={0}>asdasd</PanelRow> */}
			{[
				0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 0, 0.2, 0.4, 0.6,
				0.8, 1, 1.2, 1.4, 1.6, 1.8,
			].map((x, i) => (
				<PanelRow key={i} percent={calc(offset + Math.PI * x)}>
					..........some text...........
				</PanelRow>
			))}

			{/* <PanelRow percent={offset + (Math.PI * 2)}>asdasd</PanelRow>
			<PanelRow percent={offset + (Math.PI * 2)}>asdasd</PanelRow>
			<PanelRow percent={offset + (Math.PI * 2)}>asdasd</PanelRow>
			<PanelRow percent={offset + (Math.PI * 2)}>asdasd</PanelRow> */}
			{/* <PanelRow percent={1}>asdasd</PanelRow> */}
		</Panel>
	);
};

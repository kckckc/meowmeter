import React, { ReactNode, useState } from 'react';
import PanelOrder from './PanelOrder';

interface PanelOrderProviderProps {
	children?: ReactNode;
}

export const PanelOrderProvider: React.FC<PanelOrderProviderProps> = ({
	children,
}) => {
	const [orderValue, setOrderValue] = useState(30);

	return (
		<PanelOrder.Provider
			value={{ value: orderValue, setValue: setOrderValue }}
		>
			{children}
		</PanelOrder.Provider>
	);
};

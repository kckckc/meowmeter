import { ContextMenuWrapper } from '@kckckc/isleward-util';
import classNames from 'classnames';
import React, {
	MouseEventHandler,
	ReactNode,
	useCallback,
	useContext,
	useEffect,
	useRef,
	useState,
} from 'react';
import styles from './Panel.css';
import PanelOrder from './PanelOrder';

interface PanelHeaderProps {
	children?: ReactNode;
	contextMenu?: ReactNode;
	onClose?: () => void;
	setPos: (inputPos: { x: number; y: number }) => void;
	setZIndex: React.Dispatch<React.SetStateAction<number>>;
	touched: boolean;
	setTouched: React.Dispatch<React.SetStateAction<boolean>>;
}

export const PanelHeader: React.FC<PanelHeaderProps> = ({
	children,
	contextMenu,
	onClose,
	setPos,
	setZIndex,
	touched,
	setTouched,
}) => {
	// Dragging logic
	const ref = useRef<HTMLDivElement | null>(null);
	const [rel, setRel] = useState({
		x: 0,
		y: 0,
	});
	const [dragging, setDragging] = useState(false);

	// Most recent on top
	const { value, setValue } = useContext(PanelOrder);

	const onMouseDown = useCallback<MouseEventHandler<HTMLDivElement>>(
		(e) => {
			if (e.button !== 0) return;

			if (!ref?.current) return;

			const rect = ref.current.getBoundingClientRect();
			setDragging(true);
			setRel({
				x: e.pageX - rect.left,
				y: e.pageY - rect.top,
			});

			setZIndex((p) => {
				if (p <= value) {
					setValue(value + 1);
					return value + 1;
				}
				return p;
			});

			//stoppropagation/preventdefault?
		},
		[value, setValue, setZIndex]
	);

	useEffect(() => {
		const onMouseUp = (e: MouseEvent) => {
			setDragging(false);

			e.stopPropagation();
			e.preventDefault();
		};

		const onMouseMove = (e: MouseEvent) => {
			if (!dragging) return;

			if (!touched) {
				setTouched(true);
			}

			setPos({
				x: e.pageX - rel.x,
				y: e.pageY - rel.y,
			});
		};
		if (dragging) {
			document.addEventListener('mouseup', onMouseUp);
			document.addEventListener('mousemove', onMouseMove);
		}

		return () => {
			document.removeEventListener('mouseup', onMouseUp);
			document.removeEventListener('mousemove', onMouseMove);
		};
	}, [dragging, rel, setPos, touched, setTouched]);

	const header = (
		<div
			className={classNames(styles.header, 'hasBorderShadow')}
			ref={ref}
			onMouseDown={onMouseDown}
		>
			{children}

			<div onClick={onClose} className={styles.close}>
				X
			</div>
		</div>
	);

	if (contextMenu) {
		return (
			<ContextMenuWrapper contextMenu={contextMenu}>
				{header}
			</ContextMenuWrapper>
		);
	} else {
		return header;
	}
};

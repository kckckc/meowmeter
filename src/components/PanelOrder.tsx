import { createContext } from 'react';

export interface PanelOrderProps {
	value: number;
	setValue: (val: number) => void;
}

const PanelOrder = createContext<PanelOrderProps>({
	value: 30,
	setValue: () => {
		//stub
	},
});

export default PanelOrder;

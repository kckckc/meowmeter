import classNames from 'classnames';
import React, {
	CSSProperties,
	ReactNode,
	useCallback,
	useContext,
	useEffect,
	useLayoutEffect,
	useRef,
	useState,
} from 'react';
import styles from './Panel.css';
import { PanelHeader } from './PanelHeader';
import PanelOrder from './PanelOrder';

interface PanelProps {
	header?: ReactNode;
	children?: ReactNode;
	contextMenu?: ReactNode;
	onClose?: () => void;
	style?: CSSProperties;
	openCentered?: boolean;
	saveAs?: string;
	clamped?: boolean;
}

export const Panel: React.FC<PanelProps> = ({
	header,
	children,
	contextMenu,
	onClose,
	style,
	openCentered,
	saveAs,
	clamped = true,
}) => {
	const [pos, setPos] = useState({
		x: 0,
		y: 0,
	});
	const [zIndex, setZIndex] = useState(30);
	const [touched, setTouched] = useState(false);
	const [loaded, setLoaded] = useState(false);

	const { setValue } = useContext(PanelOrder);

	const divRef = useRef<HTMLDivElement>(null);

	const clampPos = useCallback(
		(inputPos: { x: number; y: number }) => {
			if (!clamped) return inputPos;

			const newPos = { ...inputPos };
			if (newPos.x < 2) {
				newPos.x = 2;
			}
			if (newPos.y < 2) {
				newPos.y = 2;
			}
			if (divRef.current) {
				const rect = divRef.current.getBoundingClientRect();
				if (newPos.x + rect.width > window.innerWidth - 2) {
					newPos.x = window.innerWidth - rect.width - 2;
				}
				if (newPos.y + rect.height > window.innerHeight - 2) {
					newPos.y = window.innerHeight - rect.height - 2;
				}
			}
			return newPos;
		},
		[divRef, clamped]
	);

	const setPosClamped = useCallback(
		(inputPos: { x: number; y: number }) => {
			setPos((prev) => {
				const newPos = clampPos(inputPos);
				if (newPos.x !== prev.x || newPos.y !== prev.y) {
					return newPos;
				} else {
					return prev;
				}
			});
		},
		[clampPos]
	);

	useLayoutEffect(() => {
		const onResize = () => {
			setPos((prev) => {
				return clampPos({ ...prev });
			});
		};

		window.addEventListener('resize', onResize);
		return () => {
			window.removeEventListener('resize', onResize);
		};
	}, []);

	// Save
	// TODO debounce?
	useEffect(() => {
		if (saveAs && loaded) {
			localStorage.setItem(
				`panel.${saveAs}`,
				JSON.stringify({
					x: pos.x,
					y: pos.y,
					z: zIndex,
				})
			);
		}
	}, [pos, zIndex, saveAs, loaded]);
	// Restore
	useLayoutEffect(() => {
		if (saveAs) {
			const data = localStorage.getItem(`panel.${saveAs}`);

			if (data) {
				const parsed = JSON.parse(data);
				setPos(
					clampPos({
						x: parsed?.x ?? 0,
						y: parsed?.y ?? 0,
					})
				);
				if (parsed?.z) {
					setZIndex(parsed.z);
					setValue(parsed.z);
				}
			}

			setLoaded(true);
		}
	}, [clampPos, setValue, saveAs]);

	let headerEl: ReactNode = null;
	if (header) {
		headerEl = (
			<PanelHeader
				setPos={setPosClamped}
				onClose={onClose}
				contextMenu={contextMenu}
				setZIndex={setZIndex}
				touched={touched}
				setTouched={setTouched}
			>
				{header}
			</PanelHeader>
		);
	}

	const posProps: CSSProperties =
		openCentered && !touched
			? {
					position: 'fixed',
					left: '50%',
					top: '50%',
					transform: 'translate(-50%, -50%)',
			  }
			: { left: `${pos.x}px`, top: `${pos.y}px` };

	const mergedStyle: CSSProperties = {
		...style,
		...posProps,
		zIndex,
	};

	return (
		<div
			className={classNames(styles.panel, 'hasBorderShadow')}
			style={mergedStyle}
			ref={divRef}
		>
			{headerEl}
			<div className={styles.body}>{children}</div>
		</div>
	);
};

import React, { ReactNode } from 'react';
import { percentString } from '../util/numbers';
import styles from './Panel.css';

interface PanelRowProps {
	children?: ReactNode;
	percent?: number;
}

export const PanelRow: React.FC<PanelRowProps> = ({ children, percent }) => {
	return (
		<div className={styles.row}>
			<div className={styles.rowBar}>
				<div
					className={styles.rowBarInner}
					style={{ width: percentString(percent) }}
				/>
			</div>
			<div className={styles.rowContent}>{children}</div>
		</div>
	);
};

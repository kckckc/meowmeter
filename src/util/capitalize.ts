export const capitalize = (str: string) => {
	return str
		.split(/\s+/)
		.map((s) => {
			if (s.length < 2) return s.toUpperCase();
			return s[0].toUpperCase() + s.slice(1);
		})
		.join(' ');
};

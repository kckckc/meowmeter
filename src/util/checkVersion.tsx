import React from 'react';
import { CHANGELOG_LINK } from '../consts';
import { NoticePanel } from '../panels/NoticePanel';
import { renderer } from '../renderer/renderer';
import settings from '../settings';
import styles from './checkVersion.css';

export const checkVersion = () => {
	const prevVersion = settings.getValue('version');
	if (prevVersion !== undefined && prevVersion !== __ADDONVERSION__) {
		renderer.addElement(
			'version-notice',
			<NoticePanel name="version-notice" header="MeowMeter Updated">
				<div className={styles.text}>
					MeowMeter was updated to version{' '}
					<span className={styles.bright}>v{__ADDONVERSION__}</span>.
					View the changelog{' '}
					<a
						href={CHANGELOG_LINK}
						target="_blank"
						className={styles.link}
					>
						here
					</a>
					.
				</div>
			</NoticePanel>
		);
	}
	settings.setValue('version', __ADDONVERSION__);
};

export const abbreviateNumber = (n: number): string => {
	if (n < 1e4) return '' + n.toFixed(0);
	if (n < 1e6) return (n / 1e3).toFixed(1) + 'k';
	return (n / 1e6).toFixed(1) + 'm';
};

export const percentString = (percent?: number): string => {
	return `${((percent ?? 0) * 100).toFixed(1)}%`;
};

import { Event } from '@kckckc/amalgam';
import { DamageTypeDetails } from '../tracker';

export const summarizeDamage = (events: Event[], total?: number) => {
	if (!events.length) {
		return null;
	}

	let amount = 0;
	events.forEach((event) => {
		if (event.data.type !== 'damage') return;

		const dmg = event.data.data;
		amount += dmg.amount;
	});
	let earliest = events[events.length - 1].timestamp;
	let now = Date.now();
	let elapsedMs = now - earliest;

	const dps = amount / (elapsedMs / 1000);
	const dpt = amount / (elapsedMs / 350);

	const details: DamageTypeDetails = {
		amount: amount,
		dps,
		dpt,
	};

	if (total) {
		details.percent = amount / total;
	}

	return details;
};

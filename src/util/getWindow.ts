export const getWindow = () => {
	return typeof unsafeWindow === 'undefined' ? window : unsafeWindow;
};

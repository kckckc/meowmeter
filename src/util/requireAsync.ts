import { getWindow } from './getWindow';

// RequireJS require promise wrapper
export const requireAsync = async (deps: string | string[]): Promise<any> => {
	if (typeof deps === 'string') {
		// Single
		return new Promise((res) => {
			getWindow().require([deps], (m) => {
				res(m);
			});
		});
	} else {
		// Multi
		// const [a, b, c] = requireAsync(['a', 'b', 'c']);
		return await Promise.all(deps.map((p) => requireAsync(p)));
	}
};

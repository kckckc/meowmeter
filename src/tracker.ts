import { EventStack } from '@kckckc/amalgam';

type SubscriberFn = () => void;

export type DamageTypeDetails = {
	amount: number;
	dps: number;
	dpt: number;
	percent?: number;
};

export type DamageInstanceFilter = {
	element?: string;
};

// Until changes are deployed
const transformMessage = (
	msg: OnGetDamageMessageOld
): OnGetDamageMessage | null => {
	if (msg.event) {
		// console.log(msg);
		return null;
	}

	const {
		id: target,
		masterSource: sourceMaster,
		masterId: targetMaster,
		...rest
	} = msg;

	const transformed: OnGetDamageMessage = {
		target,
		sourceMaster,
		targetMaster,
		...rest,
	};

	return transformed;
};

const onGetDamage = (msg: OnGetDamageMessageOld) => {
	const transformed = transformMessage(msg);

	if (!transformed) return;

	processEvent(transformed);
};

const stack = new EventStack();

const processEvent = (msg: OnGetDamageMessage) => {
	console.log(msg);

	stack.add({
		type: 'damage',
		data: msg,
	});

	notify();
};

// Listeners
let subscribers: SubscriberFn[] = [];
const subscribe = (sub: SubscriberFn) => {
	subscribers.push(sub);
};
const unsubscribe = (sub: SubscriberFn) => {
	subscribers = subscribers.filter((s) => s !== sub);
};
const notify = () => {
	subscribers.forEach((s) => s());
};

let interval: ReturnType<typeof setInterval>;
const init = () => {
	const events = window.addons.events;
	events.on('onGetDamage', onGetDamage);

	setInterval(() => {
		// notify subscribers
		notify();
	}, 1000);
};
const cleanup = () => {
	const events = window.addons.events;
	events.off('onGetDamage', onGetDamage);

	clearInterval(interval);
};

export default { init, cleanup, subscribe, unsubscribe, stack };

const path = require('path');
const url = require('url');
const TerserPlugin = require('terser-webpack-plugin');
const WebpackUserscript = require('webpack-userscript');
const { DefinePlugin } = require('webpack');

const isDev = process.env.NODE_ENV !== 'production';
const pkg = require('./package.json');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

const devBaseUrl = path.resolve(__dirname, 'build');
const devBaseUrlFile = url.pathToFileURL(devBaseUrl).href;

// Change these to whatever
const ADDON_ID = 'meowmeter';
const ADDON_NAME = 'MeowMeter';
const ADDON_AUTHOR = 'kckckc';
const ADDON_NAMESPACE = 'Isleward.Addon'; // ?
const ADDON_DESCRIPTION = `DPS meter addon for Isleward.`;
const ADDON_GRANT = 'none';
const ADDON_MATCH = [
	'http*://play.isleward.com/*',
	'http*://ptr.isleward.com/*',
];

const ADDON_VERSION = pkg.version;

const DOWNLOAD_BASE_URL =
	'https://gitlab.com/kckckc/meowmeter/-/raw/main/dist/meowmeter.user.js'; // Link to latest hosted [addon].user.js
const UPDATE_BASE_URL =
	'https://gitlab.com/kckckc/meowmeter/-/raw/main/dist/meowmeter.meta.js'; // Link to latest hosted [addon].meta.js

module.exports = (env) => ({
	mode: isDev ? 'development' : 'production',
	entry: path.resolve(__dirname, pkg.main),
	output: {
		path: path.resolve(__dirname, env.BUILD_DIR ?? 'build'),
		filename: `${ADDON_ID}.user.js`,
	},
	devtool: isDev ? 'inline-source-map' : false,
	devServer: {
		port: 8080,
		static: {
			directory: devBaseUrl,
		},
		devMiddleware: {
			writeToDisk: true,
		},
		headers: {
			'Access-Control-Allow-Origin': '*',
		},

		// Doesn't really work with userscripts
		hot: false,
		liveReload: false,
		client: false,
	},
	optimization: {
		minimizer: [new TerserPlugin({ extractComments: false })],
		minimize: !isDev,
	},
	plugins: [
		// new BundleAnalyzerPlugin(),
		new DefinePlugin({
			// Need to stringify it to insert "ADDONVERSION" (with quotes)
			__ADDONVERSION__: JSON.stringify(ADDON_VERSION),
		}),
		new WebpackUserscript({
			headers: {
				name: ADDON_NAME,
				namespace: ADDON_NAMESPACE,
				version: isDev ? `[version]-build.[buildNo]` : `[version]`,
				description: ADDON_DESCRIPTION,
				author: ADDON_AUTHOR,
				grant: ADDON_GRANT,
				match: ADDON_MATCH,
			},

			downloadBaseUrl: DOWNLOAD_BASE_URL,
			updateBaseUrl: UPDATE_BASE_URL,

			proxyScript: {
				baseUrl: devBaseUrlFile,
				filename: '[basename].proxy.user.js',
				enable: () => env.ADDON_PROXY === '1',
			},
		}),
	],
	resolve: {
		extensions: ['.js', '.jsx', '.ts', '.tsx'],
	},
	module: {
		rules: [
			{
				test: /\.css$/i,
				use: [
					'style-loader',
					{
						loader: 'css-loader',
						options: {
							importLoaders: 1,
							modules: true,
						},
					},
				],
			},
			{
				test: /\.tsx?$/i,
				use: 'ts-loader',
				exclude: /node_modules/,
			},
		],
	},
});
